/**
 * Created by Kewin on 19/01/15.
 */

window.livePage = {

    instruments: {},

    currentOpen: '',

    init: function() {
        var self = this;

        $('#liveTabs li').tab();

        this.getHistory(function(data) {

            self.build(data);

            self.bindUpdateIO();
        });
    },

    getHistory: function(callback) {
        $.get('/live/history', callback);
    },

    build: function(data) {

        this.destroy();

        var self = this;

        this.setHeaderInfo(data);

        _.forEach(data.instruments, function(data) {

            self.addTab(data);

        });

        window.setTimeout(function() {
console.log(self.currentOpen);
            if (self.currentOpen) {
                console.log($('#liveTabs li[data-id="' + self.currentOpen + '"]'));
                $('#liveTabs li[data-id="' + self.currentOpen + '"]').click();
            } else {
                $('#liveTabs li:first').click();
            }

        }, 500);
    },

    bindUpdateIO: function() {
        var self = this;

        window.io.on('live/tick:done', function(result) {
            self.build(result);
        });

    },

    buildCharts: function() {

    },

    addTab: function(data) {
        var name = data.name;
        var profit = data.rate;

        var color = 'color: ' + (profit >= 0 ? 'green' : 'red');

        var html = '<li class="active" data-id="' + name + '"><a data-toggle="tab">' + name + ' <span style="' + color + '"><b>(' + profit + ')</b></span></a></li>';

        $(html).appendTo('#liveTabs').click({self : this}, function(e) {

            $(this).parent().children().removeClass('real-active');

            $(this).addClass('real-active');

            e.data.self.onTabSwitch(this.getAttribute('data-id'));

        });
    },

    clearTabs: function() {
        $('#liveTabs').empty();
    },

    clearTabContent: function() {
        if (this.instruments[this.currentOpen]) {
            this.instruments[this.currentOpen].destroy();

            this.instruments[this.currentOpen] = null;
        }
    },

    onTabSwitch: function(instrumentName) {
        if (this.currentOpen === instrumentName) {
            return;
        }

        this.clearTabContent();

        var instrumentObj = this.instruments[instrumentName];

        if (!instrumentObj) {
            instrumentObj = this.instruments[instrumentName] = new Instrument(instrumentName, [], $('#liveChartsContainer'));
            instrumentObj.render();

            console.log('not present');

        } else {
            console.log('already present');
        }

        $.get('/live/' + instrumentName, function(result) {

            instrumentObj.chart.update(result);

        });

        this.currentOpen = instrumentName;
    },

    setHeaderInfo: function(data) {
        $('#liveResultContainer .totalProfit .profit').text(data.totalProfit);
        $('#liveResultContainer .resultRate').text(data.totalRate + '%');
        $('#liveResultContainer .totalProfit .balance').text(data.balanceValue);
    },

    destroy: function() {

        _.forEach(this.instruments, function(instrument) {
            instrument.destroy();
        });

        this.clearTabs();

        this.currentOpen = '';
        this.instruments = {};

        return this;

    }
};