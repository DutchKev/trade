/**
 * Created by Kewin on 19/01/15.
 */

window.emulatorPage = {

    instruments: {},

    currentOpen: '',

    init: function() {
        $('#emulatorTabs li').tab();

        this.destroy().getData(this.build.bind(this));

        return this;
    },

    getData: function(callback) {
        // TODO: Grab params from settings menu first.
        $.get('/emulate-time', callback);
    },

    addTab: function(data) {
        var name = data.name;
        var profit = data.rate;

        var color = 'color: ' + (profit >= 0 ? 'green' : 'red');

        var html = '<li class="active" data-id="' + name + '"><a data-toggle="tab">' + name + ' <span style="' + color + '"><b>(' + profit + ')</b></span></a></li>';

        $(html).appendTo('#emulatorTabs').click({self : this}, function(e) {

            $(this).parent().children().removeClass('real-active');

            $(this).addClass('real-active');

            e.data.self.onTabSwitch(this.getAttribute('data-id'));

        });
    },

    clearTabs: function() {
        $('#emulatorTabs').empty();
    },

    clearTabContent: function() {
        if (this.instruments[this.currentOpen]) {
            this.instruments[this.currentOpen].destroy();

            this.instruments[this.currentOpen] = null;
        }
    },

    onTabSwitch: function(instrumentName) {
        if (this.currentOpen === instrumentName) {
            return;
        }

        this.clearTabContent();

        var instrumentObj = this.instruments[instrumentName];

        if (!instrumentObj) {
            instrumentObj = this.instruments[instrumentName] = new Instrument(instrumentName, []);
            instrumentObj.render();
        }


        $.get('/emulate-result/' + instrumentName, function(result) {

            instrumentObj.chart.update(result);
        });

        this.currentOpen = instrumentName;
    },

    setHeaderInfo: function(data) {

        $('#emulatorResultContainer .totalProfit .profit').text(data.totalProfit);
        $('#emulatorResultContainer .resultRate').text(data.totalRate + '%');
        $('#emulatorResultContainer .totalProfit .balance').text(data.balanceValue);
    },

    build: function(data) {
        var self = this;

        this.setHeaderInfo(data);

        _.forEach(data.instruments, function(data) {

            self.addTab(data);

        });

        window.setTimeout(function() {
            $('#emulatorTabs li:first').click();
        }, 500);

    },

    destroy: function() {

        _.forEach(this.instruments, function(instrument) {
            instrument.destroy();
        });

        return this;

    }
};

