window.instruments = {};

(function(window) {
    'use strict';

    window.io = window.io();

    window.livePage.init();
    window.emulatorPage.init();


})(this);