window.charts = {};

(function(global, $, _) {
    'use strict';

    var templates = {
        container: $('#template-chartContainer').html(),
        history: _.template($('#template-historyRow').html()),
        tooltip: _.template($('template-chartTooltip').html())
    };

    global.Chart = function(opt) {
        this.$el    = $(this.template);
        this.$container = opt.$container;
        this.chart  = {};
        this.history = [];

        this.name   = opt.name;

        this.init(opt);
    };

    global.Chart.prototype = {
        template: templates.container,

        init: function(opt) {
            this.setTitle();
            this.appendToDom();

            this.create(opt.records);
        },

        appendToDom: function() {
            this.$el.appendTo(this.$container || '#chartsContainer');
        },

        setTitle: function() {
            this.$el.find('.chartTitle').text(this.name);
        },

        create: function(records) {
            if (_.isArray(records)) {
                var series = _.map(records, function(record) { return [record.timestamp, record.ask] });
                var average = _.map(records, function(record) { return [record.timestamp, record.data.average] });
            }

            var self = this;

            this.chart = new Highcharts.StockChart({
                chart: {
                    height: 600,
                    renderTo: this.$el.find('.chartBox')[0]
                },

                rangeSelector:{
                    enabled:false
                },

                plotOptions: {
                    series: {
                        animation: false
                    },

                    line: {
                        gapSize: 10
                    }
                },

                exporting: {
                    enabled: false
                },

                xAxis: {
                    type: 'datetime',
                    labels: {
                        overflow: 'justify'
                    }
                },

                tooltip:{
                    useHTML: true,
                    followPointer: true,

                    formatter: function() {
                        var time = Highcharts.dateFormat('%d/%m %H:%M', this.x);
                        var html =
                            '<p>Time: <b>' + time + '</b></p>' +
                            '<p>Value: <b>' + this.y + '</b></p>';

                        var trades = _.where(self.history, {record: {timestamp: this.x}});

                        trades.reverse().forEach(function(trade) {
                            var dirSymbol = trade.type === 'long'? '&#9650;' : '&#9660;';

                            html +=
                                '<br />' +
                                '<p><i><b>' + trade.action + '</b></i></p>' +
                                '<p>units : <b>' + trade.units + '</b></p>' +
                                '<p>type  : <b>' + trade.type + '</b> ' + dirSymbol + '</p>';

                            if (trade.action === 'sold') {
                                var profitColor = trade.profit > 0 ? 'green' : 'red';
                                html +=
                                    '<p style="color: ' + profitColor + '">profit : €' + trade.profit.toFixed(4) + '</p>';
                            }
                        });

                        return html;
                    }
                },

                series: [
                    {
                        id      : 'ask',
                        type    : 'line',
                        data    : series,
                        marker  : {
                            enabled : true,
                            radius  : 1
                        },
                        color: 'blue'
                    },
                    {
                        id      : 'short-average',
                        type    : 'line',
                        data    : average || [],
                        marker  : {
                            enabled : true,
                            radius  : 1
                        },
                        dashStyle: 'shortdot',
                        color: 'orange'
                    },

                    {
                        name: 'Linear Trendline',
                        linkedTo: 'ask',
                        showInLegend: false,
                        enableMouseTracking: false,
                        type: 'trendline',
                        algorithm: 'linear'
                    },

                    {
                        id      : 'long-average',
                        type    : 'line',
                        data    : average || [],
                        marker  : {
                            enabled : true,
                            radius  : 1
                        },
                        dashStyle: 'longdash',
                        color: 'orange'
                    },

                    {
                        id      : 'watcher-up',
                        type    : 'line',
                        data    : [],
                        marker  : {
                            enabled : true,
                            radius  : 1
                        },
                        lineWidth : 5,
                        connectEnds: false,
                        color: 'green'
                    },

                    {
                        id      : 'watcher-down',
                        type    : 'line',
                        data    : [],
                        marker  : {
                            enabled : true,
                            radius  : 1
                        },
                        lineWidth : 5,
                        connectEnds: false,
                        color: 'red'
                    },

                    {
                        id      : 'peak',
                        type    : 'flags',
                        data    : [],
                        marker  : {
                            enabled : true,
                            radius  : 1
                        },
                        color: 'black'
                    },
                    {
                        id      : 'flags',
                        type    : 'flags',
                        stackDistance: 20,
                        events: {
                            click: function() {
                                alert('click flag');
                            }
                        },
                        width : 15
                    },

                    {
                        id      : 'highLow',
                        type    : 'scatter',
                        events: {
                            click: function() {
                                alert('click scatter');
                            }
                        }
                    }
                ]
            });

            this.chart.render();

            return this;
        },

        update: function(data) {
            var typeFunc = {
                records     : 'addRecords',
                //highLow     : 'addPeaks',
                waves       : 'addWaves',
                trades      : 'setTradeHistory',
                profit      : 'updateProfit',
                rate        : 'updateProfitRate',
                scanPeriod  : 'addScanPeriod',
                scenarios   : 'addScenarioData'
                //jumper      : 'addJumperWatchers'

            };

            for (var key in data) {
                this[typeFunc[key]](data[key]);
            }

            this.chart.redraw();
        },

        updateStats: function(type, value) {
            this.$el.find('.' + type).text(value);
        },

        updateProfit: function(profit) {
            this.updateStats('profit', parseFloat(profit).toFixed(2));
        },

        updateProfitRate: function(profitRate) {
            this.updateStats('profitRate', profitRate.toFixed(2));
        },

        addRecords: function(records) {
            var pointsData  = _.map(records, function(r) { return [r.timestamp, r.ask]});
            var shortAverageData = _.map(records, function(r) { return [r.timestamp, r.data.average.short.current]});
            var longAverageData = _.map(records, function(r) { return [r.timestamp, r.data.average.long.current]});

            this.chart.get('ask').setData(pointsData);
            this.chart.get('short-average').setData(shortAverageData);
            this.chart.get('long-average').setData(longAverageData);
        },

        addPeaks: function(peaks) {
            var peakSeries  = this.chart.get('peak');

            peaks.forEach(function(peak) {
                var flagOptions = {
                    x: peak.record.timestamp,
                    y: peak.record.ask,
                    title: peak.type
                };

                peakSeries.addPoint(flagOptions, false);
            });

        },

        addScanPeriod: function(period) {
            this.chart.xAxis[0].addPlotLine({
                value: period.until,
                color: '#BF46EB',
                width: 3
            });
        },

        addHighLow: function(flags) {
            var flagSeries = this.chart.get('highLow');

            flags.forEach(function(flag) {

                var flagOptions = {
                    x: flag.record.timestamp,
                    y:flag.record.ask - (flag.record.ask * 0.007),
                    title: ' ',
                    marker: {
                        symbol: flag.type === 'high' ? "url(img/green_up.png)" : "url(img/red_down.png)"
                    }
                };

                flagSeries.addPoint(flagOptions, false);
            });
        },

        addTradeFlags: function() {
            var flagSeries = this.chart.get('flags');

            this.history.reverse().forEach(function(flag) {

                var flagOptions = {
                    x: flag.record.timestamp,
                    y:flag.record.ask,
                    title: 'trade',
                    //shape: 'circlepin',
                    style : { // text style
                        color : 'white',
                        //fontSize: '11px',
                        fontWeight: 'bold',
                        textAlign: 'center'
                    },
                    width: 150
                };

                if (flag.action === 'bought') {
                    flagOptions.title = 'B';
                    flagOptions.fillColor = '#fff';
                    flagOptions.color = '#000';
                    flagOptions.style.color = 'black';
                }

                else if (flag.action === 'sold') {
                    flagOptions.title = 'S';
                    var hasProfit = flag.profit > 0;
                    flagOptions.fillColor = hasProfit ? 'green' : 'red';
                    flagOptions.color = flag.profit > 0 ? 'green' : 'red';
                }

                flagSeries.addPoint(flagOptions, false);
            });
        },

        addJumperWatchers: function(data) {
            var seriesUp = this.chart.get('watcher-up');
            var seriesDown = this.chart.get('watcher-down');

            data.forEach(function(watcher) {

                if (watcher.type === 'high') {
                    seriesUp.addPoint([watcher.from.timestamp, watcher.from.ask]);
                    seriesUp.addPoint([watcher.until.timestamp, watcher.until.ask]);
                } else {
                    seriesDown.addPoint([watcher.from.timestamp, watcher.from.ask]);
                    seriesDown.addPoint([watcher.until.timestamp, watcher.until.ask]);
                }

            });

        },

        removePlotBands: function() {
            this.chart.xAxis[0].removePlotBand();
        },

        setTradeHistory: function(trades) {
            this.history = trades;

            var html = '';

            _.forEach(trades, function(trade) {
                var data = {
                    type: trade.type,
                    time: Highcharts.dateFormat('%d/%m %H:%M', new Date(trade.record.time)),
                    timestamp: trade.record.timestamp,
                    units: trade.units,
                    price: trade.record.ask.toFixed(2),
                    balance: trade.balance.toFixed(2)
                };

                if (trade.profit) {
                    data.profit     = trade.profit.toFixed(2);
                    data.profitColor = data.profit > 0 ? 'green' : 'red';
                }

                html += templates.history(data);
            });

            this.$el.find('.tradeHistory .rows').append(html);

            this.addTradeFlags();
        },

        addScenarioData: function(data) {
            var plotAxis = this.chart.xAxis[0];

            var scenarios = data.history;

            var colors = {
                up: 'rgba(118, 179, 61, 0.07)',
                down: 'rgba(12, 2, 244, 0.07)',
                stable: 'rgba(0,0,0,0.2)'
            };

            scenarios.forEach(function (scenario) {

                plotAxis.addPlotBand({
                    color: colors[scenario.name],
                    from: scenario.from,
                    to: scenario.until,
                    label: {
                        text: scenario.name
                    }
                });

            });
        },

        destroy: function() {
            this.chart.destroy();

            this.$el.remove();

            this.history = null;
        }
    };

})(this, this.jQuery, this._);