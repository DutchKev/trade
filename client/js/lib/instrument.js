(function(global) {
    'use strict';

    global.Instrument = function(name, records, $container) {
        this.name       = name;
        this.chart      = null;

        this.$container = $container;

        this.init();
    };

    global.Instrument.prototype = {

        init: function() {


        },

        onUpdate: function(data) {

        },

        render: function(data) {
            (_.isObject(data) || (data = {}));

            if (this.isRendered() === false) {
                this.chart = new global.Chart({records: data.records, name: this.name, $container: this.$container});
            }
        },

        isRendered: function() {
            return !!(this.chart);
        },

        destroy: function() {
            this.chart.destroy();

            this.chart = null;
        }
    };


})(this);