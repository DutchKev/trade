/**
 * Created by Kewin on 21/01/15.
 */

module.exports = {
    run: function () {
        "use strict";

        var childProcess = require("child_process");

        this._retrieveChild = childProcess.fork("./multi-core/process");

        var _finalizedData = null,
            _httpRequestArray = ["http://someurl", "http://someurl2", "http://someurl3"];

        var data = {
            "start": true,
            "interval": 60 * 60 * 1000,
            "content": _httpRequestArray
        };

        var test = function () {
            this.doStuff = function () {
                console.log("YES")
            };
        };

        this._retrieveChild.send(new test);

        console.log(global.MainFrame);

    }
};