'use strict';

var _   = require('underscore');

module.exports = {
    getWeeksAgo: function(numberOfWeeks) {
        var today = new Date();
        return today.setDate(today.getDate() - (numberOfWeeks * 7));
    },

    getMonthsAgo: function(numberOfMonths) {
        var today = new Date();
        return today.setMonth(today.getMonth() - (numberOfMonths));
    },

    getRecordsBetweenKeys: function (records, fromKey, untilKey) {
        var returnArray = [];

        for (var i = 0, len = records.length; i < len; ++i) {
            if (i >= fromKey && (!untilKey || i <= untilKey)) {
                returnArray.push(records[i]);
            }
        }

        return returnArray;
    },

    getHighest: function(records) {
        var highest = 0;
        _.forEach(records, function(record){
            if (!highest || highest.ask < record.ask) {
                highest = record;
            }
        });

        return highest;
    },

    getRecordsBetweenTimes: function(records, from, until) {
        var filteredData = [];

        for (let i = 0, len = records.length; i < len; ++i) {

            let record = records[i];

            if (record.timestamp >= from && (!until || record.timestamp <= until)) {
                filteredData.push(record);
            }
        }

        return filteredData;
    },

    getDiffPercentage: function(from, to) {
        return (to - from) / to * 100;
    },

    getAverage: function(array) {
        var total = 0;

        for (let i = 0, len = array.length; i < len; i++) {
            total += array[i];
        }

        return (total / array.length);
    },

    getAverageFromRecords: function(records, value) {
        return this.getAverage(_.map(records, value || 'ask'));
    },

    getDirection: function(records) {
        var result = 'static';

        var halfPoint   = Math.floor(records.length / 2);
        var leftTotal   = 0;
        var rightTotal  = 0;

        for (let i = 0, len = records.length; i < len; ++i) {
            if (i < halfPoint) {
                leftTotal += records[i].ask;
            } else {
                rightTotal += records[i].ask;
            }
        }

        var diffPercentage = this.getDiffPercentage(leftTotal, rightTotal);

        if (diffPercentage > -0.1 && diffPercentage < 0.1) {
            result = 'stable'
        }
        else if (leftTotal > rightTotal) {
            result = 'down';
        }
        else {
            result = 'up'
        }

        return result;
    }
};
