"use strict";


/** System **/
var numCPUs = require('os').cpus().length;

/* Config */
global.root         = __dirname + '/';
global.live         = !!(process.argv.slice(2).indexOf('live') > -1);
global.config       = require('../package.json').config;
global.accountId    = config.practise.account.id;
global.account      = {
    id: config.practise.account.id
};

/** App config **/
global.symConfig = {
    timeInterval: 'H4'
};

/* External libraries */
global._            = require('underscore');
global.logger       = require('tracer').colorConsole();
global.app          = require('express')();
global.server       = require('http').Server(app).listen(8888);
global.io           = require('socket.io')(server);
global.api          = new (require('oanda-adapter'))(config.practise.api);

/* App libraries */
global.tools        = require(global.root + 'tools/tools');
global.Frame        = require('./modules/calculate/frame');

global.db           = new (require('./modules/data/init'));

app.use(require('express').static(process.cwd() + '/../client'));

/** Routes **/
var routes          = require('./routes/routes');
app.get('/', routes.home);
app.get('/live/history', routes.getLiveHistory);
app.get('/live/:name', routes.getLiveInstrument);
app.get('/emulate-time', routes.emulateTime);
app.get('/emulate-result/:name', routes.getEmulatedInstrument);

/*
var corePlay = require('./multi-core/play');
corePlay.run();
*/

global.topInstruments   = ['xag_usd', 'bco_usd', 'eur_nok', 'gbp_pln', 'aud_sgd'];
global.topInstruments   = ['xag_usd'];

require('./modules/calculate/startup').init(function() {

    // Create 'main-frame'
    global.MainFrame = new Frame({live: live});

    // Bind tick event for real-time browser update (IO)
    global.MainFrame.on('tick:done', function() {

        // Emit the tick result to the browser
        global.io.emit('live/tick:done', this.createResult().overView);

    });

    // Get history
    global.db.getHistory({from: 0, until: 0}, function(data) {

        // Insert history into main
        global.MainFrame.setHistory(data);

        global.db.on('update', function(data) {

            console.log('update @ ' + new Date());

            global.MainFrame.tick(data);

        });

        console.log('system ready!'.green);

        if (!live) {

            var result = require('./modules/emulator/emulator').lastResult.overView;

            console.log(result);
        }

        require('./modules/data/ticker').start();


        //FakeUpdater.run(global.MainFrame);
    });

});

global.io.on('connection', function(socket) {});

var FakeUpdater = {

    isRunning: false,

    interval: null,

    loop: function() {

    },

    run: function(frame) {
        if (this.isRunning) {
            return;
        }

        this.isRunning = true;

        this.turnTradingOn(frame);

        setInterval(function() {

            global.db.getFakeTick(_.clone(topInstrumentsTemp));

        }, 5000);
    },

    turnTradingOn: function(frame) {

        _.forEach(frame.instruments, function(instrument) {

            var doAll = false;

            var isTop = _.contains(global.topInstruments, instrument.name);

            if (isTop || doAll) {
                instrument.doTrades = true;
            } else {
                instrument.doTrades = false;
            }
        });

    }
};