'use strict';

module.exports = {
    home: function(req, res) {
        res.sendfile(__dirname + '/index.html');
    },

    /*** LIVE ***/
    getLiveHistory: function(req, res) {
        var result = global.MainFrame.createResult();

        res.json(result.overView);
    },

    getLiveInstrument: function(req, res) {
        var result = global.MainFrame.createResult();

        res.json(result.instruments[req.params.name]);
    },

    /*** EMULATOR ***/
    emulateTime: function(req, res) {
        var emulator = require('../modules/emulator/emulator');

        res.json(emulator.lastResult.overView);
    },

    getEmulatedInstrument: function(req, res) {
        var emulator = require('../modules/emulator/emulator');

        res.json(emulator.lastResult.instruments[req.params.name]);
    }
};