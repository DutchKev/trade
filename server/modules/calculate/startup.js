var spin    = require('term-spinner');

module.exports = {
    callback: null,

    interval: null,

    spinner: null,

    events: {
        account: false,
        database: false,
        history: false
        //frame: false
    },

    init: function(callback) {
        this.callback = callback;

        var self = this;

        this.connectDB(function() {

            self.checkReady('database');

            self.refreshDB(function() {

                self.checkReady('history');

                self.getAccount(function() {

                    self.checkReady('account');

                });

            });

        });
    },

    loopOverEvents: function() {

        var toDo = _.findWhere(this.events, false);

        if (toDo) {



        }

    },

    updateConsole: function(text, state) {
        if (state === 'busy') {
            var self = this;

            this.spinner = spin.new();

            this.interval = setInterval(function () {
                self.spinner.next();
                process.stdout.clearLine();
                process.stdout.cursorTo(0);
                process.stdout.write([text, self.spinner.current].join(" "));
            }, 100);

        } else if (state === 'done') {
            process.stdout.clearLine();
            process.stdout.cursorTo(0);
            clearInterval(this.interval);
            this.spinner = null;

            console.log(text + ':' + ' done'.green);
        }
    },


    // Get the correct account data
    getAccount: function(callback) {
        var self = this;

        //this.updateConsole('account', 'busy');

        global.api.getAccount(global.accountId, function(error, data) {
            data.balance = data.balance;

            global.account = data;

            callback()
        });
    },

    connectDB: function(callback) {
        var self = this;

        global.db.connect(function(err) {
            if (!err) {
                callback();
            }
        });
    },

    refreshDB: function(callback) {
        return callback();

        global.db.updateHistory(function(err) {
            if (!err) {
                callback();
            }
        });
    },

    checkReady: function(eventName) {
        this.updateConsole(eventName, 'done');

        this.events[eventName] = true;

        var numberDone = 0;

        for (var event in this.events) {
            if (this.events[event] === true) {
                numberDone++;
            }
        }

        if (numberDone === Object.keys(this.events).length) {

            this.callback();

        }
    }
};