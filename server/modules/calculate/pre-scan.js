var _       = require('underscore');
var tools   = require('./tools');
var Frame   = require('./frame');

var count = 0;
/*
module.exports = {
    setTopInstruments: function() {
        var result = this.findBestParams();

        this.setInstrumentsParams(result);
        this.setGlobalTopInstruments(result);
    },

    setInstrumentsParams: function(results) {
        var instruments = global.MainFrame.instruments;

        _.forEach(results, function(result) {
            instruments[result.name].highLowParam = result.param;
        });
    },

    setGlobalTopInstruments: function(result) {
        global.topInstruments = _.first(result, 15);
    },

    findBestParams: function() {
        var loops = 10;

        var highest = {};

        var from  = tools.getWeeksAgo(2); // twoWeeksAgo

        var until = tools.getWeeksAgo(1); // oneWeekAgo

        while (loops--) {
            var param = loops / 10; // is the diff percentage. (0.001 > 1)

            var result = this.findBestInstruments(0, from, until, param);

            _.forEach(result, function(data) {
                if (!highest[data.name] || data.rate > highest[data.name].rate) {
                    highest[data.name] = _.extend({param: param}, data);
                }
            });

            //console.log(loops)
        }

        var sortedObject = _.sortBy(highest, function(val) {
            return -val.rate;
        });

        return sortedObject;
    },

    findBestInstruments: function(limit, from, until, param) {
        var highest = [];

        _.forEach(global.MainFrame.instruments, function(instrumentObj) {
            var scanResult = this.scanInstrument(instrumentObj.name, from, until, param);

            highest.push({
                rate: scanResult.rate,
                profit: scanResult.profit,
                name: instrumentObj.name
            });
        }, this);

        var sorted = highest.sort(function (a, b) {
            return a.profit - b.profit;
        }).reverse(); // Reverse to get high to low

        return sorted;
    },

    scanInstrument: function(instrumentName, from, to, param) {

        var frame = new Frame({id: 'instrumentScan'});

        var instrument = frame.addInstrument(instrumentName, [], true);

        instrument.highLowParam = param;

        var fakeData = global.MainFrame.instruments[instrumentName].getRecordsBetweenTimes(from, to);

        for (var i = 0, len = fakeData.length; i < len; ++i) {
            var obj = {};
            obj[instrumentName] = fakeData[i];

            frame.tick(obj);
        }

        var result = {
            rate: frame.tradeEngine.rate,
            profit: frame.tradeEngine.profit.toFixed(2),
            trades: frame.tradeEngine.history,
            highLow: instrument.highLow,
            records: fakeData
        };

        frame.destroy();

        return result;
    }
};
*/