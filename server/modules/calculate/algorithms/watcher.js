'use strict';

var _           = require('underscore');
var tools       = require(global.root + 'tools/tools');
var _BaseClass  = require(global.root + 'class/_base');

/**
 * Default watcher params.
 * @type {{maxSteps: number, trackAfterCrossingAverage: boolean, threshold: boolean, takeProfitPercentage: number, takeLossPercentage: number}}
 */
var defaults = {
    maxSteps: 0,
    trackAfterCrossingAverage: false,
    threshold: false,
    takeProfitPercentage: false,
    takeLossPercentage: false,
    positiveRayTracingLength: false

};

/**
 *
 * @param params
 * @constructor
 */
var Watcher = function(params) {
    this.type       = params.type;
    this.records    = [params.startRecord];
    this.currentProfitPercentage = 0;
    this.crossedAverage = false;

    this.options = _.extend({}, defaults, params);
};

/**
 *
 * @type {{init: Function, tick: Function, checkCrossedAverage: Function, setProfitPercentage: Function, checkProfitLimit: Function, checkUp: Function, checkDown: Function}}
 */
Watcher.prototype = Object.create({}, {
    init: {
        value: function() {

        }
    },

    tick: {
        /**
         *
         * @param record
         * @returns {*}
         */
        value: function(record) {
            this.setCrossedAverage(record);

            this.records.push(record);

            var winLimitResult = this.setProfitPercentage(record).checkProfitLimit();

            if (winLimitResult) {
                return {
                    type: 'success',
                    record: record
                }
            }

            var lossLimitResult = this.checkLossLimit();

            if (lossLimitResult) {
                return {
                    type: 'fail',
                    record: record
                }
            }

            var maxStepsLimitResult = this.checkMaxSteps(record);

            if (maxStepsLimitResult) {
                return maxStepsLimitResult;
            }

            if (this.type === 'high') {
                return this.checkUp(record);
            } else if (this.type === 'low') {
                return this.checkDown(record);
            }

        }
    },

    setCrossedAverage: {
        /**
         *
         * @param record
         */
        value: function(record) {
            if (this.crossedAverage || this.records.length === 0) {
                return;
            }

            var previousRecord = _.last(this.records);

            if (this.type === 'high' && previousRecord.data.average.short.distance < 0 && record.data.average.short.distance >= 0) {
                this.crossedAverage = true;
            }
            else
            if (this.type === 'low' && previousRecord.data.average.short.distance > 0 && record.data.average.short.distance <= 0) {
                this.crossedAverage = true;
            }
        }
    },

    setProfitPercentage: {
        value: function(record) {
            var result  = tools.getDiffPercentage(this.records[0].ask, record.ask);

            if (this.type === 'low') {
                result = -result;
            }

            this.currentProfitPercentage = result;

            return this;
        }
    },

    checkProfitLimit: {
        value: function() {
            // If the option is not enabled +
            // If it may only succeed after crossing the average (if enabled)
            if (!this.options.takeProfitPercentage || (this.trackAfterCrossingAverage && !this.crossedAverage)) {
                return;
            }

            if (this.currentProfitPercentage >= this.options.takeProfitPercentage) {
                return true;
            }
        }
    },

    checkLossLimit: {
        value: function() {
            if (this.currentProfitPercentage >= 0) {
                return;
            }

            if (this.options.takeLossPercentage && Math.abs(this.currentProfitPercentage) >= this.options.takeLossPercentage) {
                return true;
            }
        }
    },

    checkUp: {
        /**
         *
         * @param record
         * @returns {{type: string, record: *}}
         */
        value: function(record) {
            if (this.options.trackAfterCrossingAverage && !this.crossedAverage) {
                return;
            }

            var dir = record.data.direction.dir;

            if (dir === 'down' || dir === 'halt') {

                if (record.data.direction.amount > this.options.threshold) {
                    return {
                        type: 'success',
                        record: record
                    }
                }
            }
        }
    },

    checkDown: {
        /**
         *
         * @param record
         * @returns {{msg: string, type: string, record: *}}
         */
        value: function(record) {
            if (this.options.trackAfterCrossingAverage && !this.crossedAverage) {
                return;
            }

            var dir = record.data.direction.dir;

            if (dir === 'up' || dir === 'halt') {

                if (record.data.direction.amount > this.options.threshold) {
1
                    if (!this.options.takeProfitPercentage || this.currentProfitPercentage >= this.options.takeProfitPercentage) {

                        return {
                            msg: 'changed direction + amount over threshold (' + record.data.direction.amount + ' > ' + this.options.threshold + ')',
                            type: 'success',
                            record: record
                        }

                    }
                }

            }
        }
    },

    checkMaxSteps: {
        /**
         *
         * @param record
         * @returns {{msg: string, type: string, record: *}}
         */
        value: function(record) {

            if (this.options.maxSteps && this.options.maxSteps <= this.records.length) {

                return {
                    msg: 'changed direction + amount over threshold (' + record.data.direction.amount + ' > ' + this.options.threshold + ')',
                    type: 'fail',
                    record: record
                }

            }

        }
    },

    destroy: {
        value: function() {
            for(let name in this) {
                if (this.hasOwnProperty(name)) {
                    this[name] = null;
                }
            }
        }
    }
});

module.exports = Watcher;