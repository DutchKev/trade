"use strict";

var _       = require('underscore');
var tools   = require('../../../tools/tools');

var counter = 0;
/*
var Wave = function wave (controller) {
    this.controller = controller;

    this.isOpen     = false;

    this.data = {
        records: [],
        from: 0,
        until: 0,
        length: 0,
        highest: {},
        completion: 0
    };
};

Wave.prototype = {
    open: function(record) {
        this.isOpen = true;

        this.data.from = record.timestamp;
    },
    tick: function(record) {
        if (!this.isOpen) {
            this.open(record);
        }

        this.data.records.push(record);

        this.updateStats();
    },

    updateStats: function() {
        this.data.length    = this.data.records.length;
        this.data.until     = _.last(this.data.records).timestamp;

        this.setHighest();
        this.setPercentComplete();
    },

    setHighest: function(record) {
        this.data.highest = _.max(this.data.records, 'ask');
    },

    setPercentComplete: function() {

        var a = this.data.length;
        var b = this.controller.averageLength;
        var c = (a / b) * 100;

        this.data.completion = c;
    },

    close: function(record) {
        this.data.records   = this.data.records.filter(function(r) {return r.timestamp < record.timestamp});

        this.updateStats();
    }
};
*/

var WaveFinder = function(instrument) {
    this.instrument = instrument;

    this.current = {
        up: false,
        down: false
    };

    //this.history    = [];
    this.history      = [];
};

WaveFinder.prototype = {

    tick: function(record) {
        this.current = {
            up: false,
            down: false
        };

        this.scanPeaks('up', record);
        this.scanPeaks('down', record);
    },

    scanPeaks: function(direction, compareTo) {
        var waveLength = 15;

        var records = this.instrument.records;

        if (records.length < waveLength) {
            return false;
        }

        var currentDistanceFromAverage = compareTo.data.average.long.distance;

        // Check if the record is really 'above' or 'under' a long range
        if (direction === 'up' && currentDistanceFromAverage < 0 ||
            direction === 'down' && currentDistanceFromAverage > 0) {

            return false;

        }

        records = _.last(records, waveLength);

        var currentDistance = compareTo.ask;

        var i = records.length;

        while(i--) {
            if (direction === 'up' && records[i].ask > currentDistance) {
                return false;
            } else if (direction === 'down' && records[i].ask < currentDistance) {
                return false;
            }
        }

        this.current[direction] = true;
        this.history.push({type: direction, record: compareTo});

        return true;
    },

    scanPeaks2: function(direction, compareTo) {
        var waveLength = 50;

        var records = this.instrument.records;

        if (records.length < waveLength) {
            return false;
        }

        var currentDistanceFromAverage = compareTo.data.average.short.distance;

        // Check if the record is really 'above' or 'under' a long range
        if (direction === 'up' && currentDistanceFromAverage < 0 ||
            direction === 'down' && currentDistanceFromAverage > 0) {

            return false;

        }

        var result = true;

        records = _.last(records, waveLength);

        var currentDistance = compareTo.data.average.short.distance;

        var i = records.length;

        while(i--) {

            let distanceFromAverage = records[i].data.average.short.distance;

            if (direction === 'up' && distanceFromAverage > currentDistance) {
                result = false;
                break;
            } else if (direction === 'down' && distanceFromAverage < currentDistance) {
                result = false;
                break;
            }
        }

        if (result) {
            this.current[direction] = true;
            this.history.push({type: direction, record: compareTo});
        }

        return result;
    }

};

module.exports = WaveFinder;