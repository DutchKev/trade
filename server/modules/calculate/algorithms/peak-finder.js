var _       = require('underscore');
var tools   = require('../tools');

var PeakFinder = function(instrument) {
    this.instrument = instrument;
    this.history    = [];

    this.averageLength = 0;
};

PeakFinder.prototype = {
    setAverageStepsLength: function () {
        var waves = this.history;

        var firstWave       = this.history[0];
        var firstWaveTime   = firstWave.timestamp;
        var lastWave        = _.last(this.history);
        var lastWaveTime    = lastWave.timestamp;

        var diff = lastWaveTime - firstWaveTime;

        // In seconds
        this.averageLength = (((((diff / waves.length)) / 1000) / 60) / 60).toFixed(2) + 'Hours';
    },

    updateStats: function() {
        this.setAverageStepsLength();
    },

    scan: function() {
        var lastWave = _.last(this.history);

        if (!lastWave) {
            return;
        }
    },

    scanFull: function () {
        var waveLength = 25;

        var records = this.instrument.records;

        // Scan from the last wave.
        var fromTime = this.history.length ? this.history[this.history.length-1].timestamp : 0;

        _.forEach(records, function (record, key) {
            if (record.timestamp <= fromTime || (key - waveLength) < 0 || (key + waveLength) > records.length) {
                return;
            }

            var result = true;

            var leftArray = tools.getRecordsFromUntil(records, (key - waveLength), key);
            _.forEach(leftArray, function (leftRecord) {
                if (leftRecord.data.average.distance > record.data.average.distance) {
                    result = false;
                }
            });

            // Do a pre-check. If left fails, there is no point waisting effort on the right side.
            if (!result) {
                return;
            }

            var rightArray = tools.getRecordsFromUntil(records, key, (key + waveLength));
            _.forEach(rightArray, function (rightRecord) {
                if (rightRecord.data.average.distance > record.data.average.distance) {
                    result = false;
                }
            });

            if (result) {
                this.history.push(record);
                this.updateStats();
            }

        }, this);

    }
};

module.exports = PeakFinder;
