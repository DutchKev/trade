"use strict";

var _       = require('underscore');
var Watcher = require('../algorithms/watcher');

var JumperLowScenario = function(instrument) {
    this.buy        = null;
    this.sell       = null;
    this.openTrade  = null;

    this.instrument = instrument;
    this.history    = [];
    this.watcherHistory  = [];
};
var count = 0;
JumperLowScenario.prototype = {

    constructor: JumperLowScenario,

    tick: function (record) {

        // Reset buy parameter, so that it gives a 'fresh' opinion.
        this.buy = null;

        if (this.watcher) {
            var result = this.watcher.tick(record);

            if (result) {

                if (result.type === 'success') {

                    this.onPoint(record);

                } else {

                    this.onFail(record);

                }
            }
        } else {

            if (record.data.average.short.distance < -0.1) {
            //if (this.instrument.scanner.waveFinder.current.down) {

                this.startWatcher(record);

            }
        }
    },

    startWatcher: function(record) {
        this.watcher = new Watcher({
            type: 'low',
            startRecord: record,
            maxSteps: 5,
            threshold: 0.05
        });
    },

    onPoint: function (record) {

        this.watcherHistory.push({
            type: this.watcher.type,
            from: this.watcher.records[0],
            until: record
        });

        if (this.openTrade) {

            this.sell = {
                record: record
            };

        } else {

            if (record.data.average.short.distance < -0.2) {

                this.buy = {
                    type: 'low',
                    record: record
                };
            }
        }
    },

    onFail: function (record) {
        /*
        this.watcherHistory.push({
            type: this.watcher.type,
            from: this.watcher.records[0],
            until: record
        });
        */

        this.watcher.destroy();
        this.watcher = null;

        if (this.openTrade) {

            this.sell = {
                record: record
            };
        }
    },

    setBuy: function () {

    },

    setSell: function () {

    },

    onBought: function (order) {
        this.buy = null;
        this.openTrade = order;

        this.watcher = new Watcher({
            type: 'high',
            startRecord: order.record,
            trackAfterCrossingAverage: true,
            //takeProfitPercentage: 0.8,
            //takeLossPercentage: 1,
            threshold: 0.05,
            maxSteps: 40
        });
    },

    onSold: function () {
        this.openTrade = null;
        this.watcher = null;
        this.sell = null;
    },

    destroy: function () {
        for (let name in this) {
            if (this.hasOwnProperty(name)) {
                this[name] = null;
            }

        }
    }
};

module.exports = JumperLowScenario;