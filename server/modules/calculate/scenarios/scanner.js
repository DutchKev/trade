"use strict";

var _           = require('underscore');
var tools       = require('../../../tools/tools');
var JumperHigh  = require('./jumper-high');
var JumperLow   = require('./jumper-low');
var WaveFinder  = require('../algorithms/wave-analyzer');

var Scanner = function(instrument) {
    this.instrument     = instrument;
    this.buy            = null;
    this.sell           = null;

    this.jumperHigh     = new JumperHigh(this.instrument);
    this.jumperLow      = new JumperLow(this.instrument);
    this.waveFinder     = new WaveFinder(this.instrument);

    this.activeScenario    = null;
    this.activeScenarios   = [this.jumperLow];
    this.scenarioHistory   = [];
};

Scanner.prototype = {
    tick: function(record) {
        this.buy = null;

        this.waveFinder.tick(record);

        this.setScenarios();

        _.forEach(this.activeScenarios, function(scenario) {
            scenario.tick(record);

            if (scenario.buy) {

                this.onBuyPoint(scenario.buy);

            }
            else
            if (scenario.sell) {

                this.onSellPoint(scenario.sell);

            }
        }, this);

    },

    getDirection: function() {
        var stepsLength = 30,
            lowPoints   = _.where(_.last(this.waveFinder.history, stepsLength), {type: 'down'}).length,
            highPoints  = _.where(_.last(this.waveFinder.history, stepsLength), {type: 'up'}).length;

        if (lowPoints > (highPoints * 1.3)) {
            return 'down'
        }

        if (highPoints > (lowPoints * 1.3)) {
            return 'up'
        }

        return 'none';
    },

    getDirection2: function() {
        var stepsLength = 5,
            lastPoints  = _.last(this.waveFinder.history, stepsLength);


        if (_.every(lastPoints, {type: 'down'})) {
            return 'down'
        }

        if (_.every(lastPoints, {type: 'up'})) {
            return 'up'
        }

        return 'none';
    },

    setScenarios: function() {
        var checkLength = 30;

        if ((this.activeScenario && this.activeScenario.openTrade) ||
            this.instrument.records.length < checkLength) {
            return;
        }

        var direction = this.getDirection();

        switch (direction) {
            case 'down':
                this.setScenario(JumperHigh);
                break;
            case 'up':
                this.setScenario(JumperLow);
                break;
            default:
                this.destroyScenario();
        }

        this.setHistory(direction);

    },

    setScenario: function(Scenario) {
        /*
        if (!(this.activeScenario instanceof Scenario)) {
            this.destroyScenario();
            this.activeScenario = new Scenario(this.instrument);

            this.onSwitch()
        }
        */
    },

    destroyScenario: function() {
        if (this.activeScenario) {
            this.activeScenario.destroy();
            this.activeScenario = null;
        }
    },

    setHistory: function(name) {
        var prev = _.last(this.scenarioHistory);
        var lastRecord = _.last(this.instrument.records);

        if (prev) {
            prev.steps++;
            prev.until = lastRecord.timestamp;
        }

        if (!prev || prev.name !== name) {

            this.scenarioHistory.push({
                steps: 1,
                name: name,
                from: lastRecord.timestamp
            });

            this.onSwitch(name);
        }
    },

    onSwitch: function(newDirection) {
        if (this.openTrade) {
            this.sell = _.last(this.instrument)
        }
    },

    onBuyPoint: function(buyPoint) {
        this.buy = buyPoint;
    },

    onBought: function(order) {
        this.openTrade = order;

        _.forEach(this.activeScenarios, function(scenario) {

            scenario.buy && scenario.onBought(order);

        });
    },

    onSellPoint: function(sellPoint) {
        this.sell = sellPoint;
    },

    onSold: function() {
        this.sell = null;

        _.forEach(this.activeScenarios, function(scenario) {

            scenario.sell && scenario.onSold();

        });
    }
};

var x = [
    {
        time: 12345678,
        high: 2,
        low: 1
    },

    {
        time: 12345680,
        high: 3,
        low: 2
    },

    {
        time: 12345682,
        high: 2,
        low: 2
    }
]

module.exports = Scanner;