'use strict';

var _       = require('underscore');
var Watcher = require('../algorithms/watcher');

var JumperHighScenario = function(instrument) {
    this.buy        = null;
    this.sell       = null;
    this.openTrade  = null;

    this.instrument = instrument;
    this.history    = [];
    this.watcherHistory  = [];
};

JumperHighScenario.prototype = {
    constructor: JumperHighScenario,

    tick: function(record) {

        // Reset buy parameter, so that it gives a 'fresh' opinion.
        this.buy = null;

        if (this.watcher) {

            var result = this.watcher.tick(record);

            if (result) {

                if (result.type === 'success') {

                    this.onSuccess(record);

                } else {

                    this.onFail(record);

                }

            }
        } else {

            if (this.instrument.scanner.waveFinder.current.up) {

                this.startWatcher(record);

            }
        }
    },

    startWatcher: function(record) {
        this.watcher = new Watcher({
            type: 'high',
            startRecord: record,
            maxSteps: 5,
            threshold: 0.1
        });
    },

    onSuccess: function(record) {

        this.watcherHistory.push({
            type: this.watcher.type,
            from: this.watcher.records[0],
            until: record
        });

        if (this.openTrade) {

            this.sell = {
                record: record
            };

            this.watcher.destroy();
            this.watcher = null;

        } else {

            if (record.data.average.short.distance > 0.1) {

                this.buy = {
                    type: 'high',
                    record: record
                };
            }
        }
    },

    onFail: function(record) {
        this.watcherHistory.push({
            type: this.watcher.type,
            from: this.watcher.records[0],
            until: record
        });

        this.watcher.destroy();
        this.watcher = null;

        if (this.openTrade) {
            this.sell = {
                record: record
            };
        }
    },

    setBuy: function() {

    },

    setSell: function() {

    },

    onBought: function(order) {
        this.buy = null;
        this.openTrade  = order;

        this.watcher = new Watcher({
            type: 'low',
            startRecord: order.record,
            trackAfterCrossingAverage: true,
            //takeProfitPercentage: 0.8,
            //takeLossPercentage: 1,
            threshold: 0.05,
            maxSteps: 40
        });
    },

    onSold: function() {
        this.openTrade = null;
        this.watcher = null;
        this.sell = null;
    },

    destroy: function() {
        for (let name in this) {
            if (this.hasOwnProperty(name)) {
                this[name] = null;
            }
        }
    }
};

module.exports = JumperHighScenario;