'use strict';

var _           = require('underscore');
var _BaseClass  = require(global.root + 'class/_base');
var Scanner     = require('../scenarios/scanner');

var Instrument = function(name, records, tradeEngine, doTrades) {
    this.name           = name;
    this.tradeEngine    = tradeEngine;
    this.doTrades       = doTrades || false;


    this.openTrade      = false;
    this.profit         = 0;

    this.scanner        = new Scanner(this);

    this._records       = [];

    if (records) {
        this.update(records);
    }
};

Instrument.prototype = Object.create(_BaseClass, {

    update: {
        value: function(data) {
            this.records = data;
        }
    },

    performTrade: {
        value: function(callback) {
            if (!this.scanner.buy) {
                throw new Error('Instrument received permission to trade, but there is no known trade');
            }

            var self = this;
            var tradeRequest = this.scanner.buy;

            var type = tradeRequest.type === 'low' ? 'long' : 'short';

            var trade = this.tradeEngine.buy(this.name, type, tradeRequest.record, function(err, trade) {
                if (err) {

                    callback(err);

                } else {

                    self.scanner.onBought(trade);

                    self.openTrade = trade;

                    callback(null, self.openTrade);
                }

            });
        }
    },

    getRecordsBetweenSteps: {
        value: function(steps) {
            return _.last(this.records, steps);
        }
    },

    sellOpenTrade: {
        value: function(callback) {
            var self = this;

            this.tradeEngine.sell(this.name, this.scanner.sell.record, function(err, result) {

                if (err) {

                    callback(err, null);

                } else {

                    self.profit += result;
                    self.openTrade = null;

                    self.scanner.onSold();

                    callback(null, result);

                }
            });
        }
    },

    setAverage: {
        value: function(record) {
            var shortAverageLength  = 20;
            var longAverageLength   = 80;

            // Long average.
            var longRecords = _.last(this.records, longAverageLength);
            longRecords.push(record);
            var longAverage = tools.getAverageFromRecords(longRecords);

            // Short average.
            var shortRecords = _.last(longRecords, shortAverageLength);
            shortRecords.push(record);
            var shortAverage = tools.getAverageFromRecords(shortRecords);

            var result = {
                short: {
                    current: shortAverage,
                    distance: tools.getDiffPercentage(shortAverage, record.ask)
                },
                long: {
                    current: longAverage,
                    distance: tools.getDiffPercentage(longAverage, record.ask)
                }
            };

            return result;
        }
    },

    getDirection: {
        value: function(record) {
            var last    = this.records[this.records.length-1];
            var result  = {
                dir: 'halt'
            };

            if (last) {
                result = {
                    dir: record.ask >= last.ask ? 'up' : 'down',
                    amount: Math.abs(tools.getDiffPercentage(last.ask, record.ask))
                };
            }

            return result;
        }
    },

    records: {
        get: function() {
            return this._records;
        },

        set: function(records) {

            (_.isArray(records) || (records = [records]));

            for (let i = 0, len = records.length; i < len; ++i) {
                let record = records[i];


                if (typeof record !== 'object') {
                    throw new Error('new record is not an object, will not insert.');
                }

                record.data = {
                    average     : this.setAverage(record),
                    direction   : this.getDirection(record)
                };

                // Add record to the beginning of the array.
                this._records.push(record);

                this.scanner.tick(record);
            }

        }
    },

    destroy: {
        value: function() {
            this.scanner    = null;
            this.openTrade  = null;

            this._records   = null;
        }
    }
});

module.exports = Instrument;