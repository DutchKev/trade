'use strict';

var _BaseClass  = require('../../class/_base');
var Instrument  = require('./instrument/instrument');
var TradeEngine = require('./trade-engine');

var Frame = function(opt) {
    (_.isObject(opt) || (opt = {}));

    this.id = opt.id || '';

    this.tradeEngine = null;
    this.instruments = {};
    this.live = opt.live;

    this.counts = 0;

    this.init();
};

Frame.prototype = Object.create(_BaseClass, {
    init: {
        value: function() {
            this.tradeEngine = new TradeEngine({live: this.live});
        }
    },

    addInstrument: {
        value: function(name, records, trade) {
            return (this.instruments[name] = new Instrument(name, records || [], this.tradeEngine, trade));
        }
    },

    setHistory: {
        /**
         *
         * @param history
         */
        value: function(history) {
            for (let instrumentName in history) {

                let instrument = this.instruments[instrumentName];

                if (typeof instrument === 'undefined') {

                    this.addInstrument(instrumentName, history[instrumentName], true);

                } else {

                    instrument.records = history[instrumentName];

                }

            }
        }
    },

    tick: {
        /**
         *
         * @param data
         */
        value: function(data) {
            var self = this;

            //console.time('sdf');
            // Update every instrument with new data.
            for (let instrumentName in data) {
                this.instruments[instrumentName].update(data[instrumentName]);
            }
            //console.timeEnd('sdf');

            // First check for anything to sell
            this.tickSell(function() {

                // Then check for anything to buy
                self.tickBuy(function() {

                    // Broadcast about finishing this tick
                    self.trigger('tick:done');
                });
            });
        }
    },

    tickBuy: {
        /**
         * Compare all instruments that wants to buy
         * Let the 'highest score' perform the trade.
         */
        value: function(callback) {
            var hasBought = false;
            var instruments = this.instruments;

            for (let name in instruments) {
                let instrument = instruments[name];

                if (instrument.doTrades && instrument.scanner.buy) {

                    hasBought = true;

                    instrument.performTrade(callback);
                }
            }

            if (hasBought === false) {
                callback();
            }
        }
    },

    tickSell: {
        /**
         * Find any instrument that wants to sell
         *
         * @param callback
         */
        value: function(callback) {
            var hasSold = false;

            _.forEach(this.instruments, function(instrument) {
                if (instrument.openTrade && instrument.scanner.sell) {

                    hasSold = true;

                    instrument.sellOpenTrade(callback);
                }
            });

            if (hasSold === false) {
                callback()
            }
        }
    },

    createResult: {
        /**
         *
         * @param frame
         * @param scanPeriod
         * @returns {{totalProfit: *, totalRate: *, balanceValue: *, instruments: *}}
         */
        value: function(frame, scanPeriod) {

            frame = frame || this;

            // Global results
            var result   = {
                totalRate   : frame.tradeEngine.rate.toFixed(2),
                totalProfit : frame.tradeEngine.profit.toFixed(2),
                balanceValue: parseFloat(frame.tradeEngine.balanceValue).toFixed(2),
                instruments : {}
            };

            // Instrument results (stored)
            _.forEach(frame.instruments, function(instrument, instrumentName) {

                if (!frame.tradeEngine.history[instrumentName] || frame.tradeEngine.history[instrumentName].length === 0) {
                    return;
                }

                result.instruments[instrumentName] = {
                    records         : instrument.records,
                    trades          : frame.tradeEngine.history[instrumentName],
                    rate            : (instrument.profit / frame.tradeEngine.startBalance) * 100,
                    profit          : instrument.profit.toFixed(2),
                    scanPeriod      : scanPeriod,
                    jumper          : (function() {

                        var result  = [];

                        instrument.scanner.jumperHigh.watcherHistory.forEach(function(w) {
                            result.push(w);
                        });

                        instrument.scanner.jumperLow.watcherHistory.forEach(function(w) {
                            result.push(w)
                        });

                        return result;

                    })(),
                    scenarios       : {
                        history         : instrument.scanner.scenarioHistory
                    },

                    highLow         : instrument.scanner.waveFinder.history

                };
            });

            //result.instruments = _.sortBy(result.instruments, function(instr) { return -instr.rate;})

            this.lastResult = result;

            result.overView = {
                totalProfit: result.totalProfit,
                totalRate: result.totalRate,
                balanceValue: result.balanceValue,
                instruments: _.map(result.instruments, function(instrument, key) {

                    return {
                        name: key,
                        rate: parseFloat(instrument.rate).toFixed(2)
                    }

                })
            };

            return result;

        }
    },

    destroy: {
        /**
         * clean up the shit
         */
        value: function() {

            _.forEach(this.instruments, function(instr) {
                instr.destroy();
            });

            this.instruments = null;
            this.tradeEngine = null;
        }
    }
});

module.exports = Frame;
