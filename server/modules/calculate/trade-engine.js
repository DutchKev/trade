var _ = require('underscore');

var Trade   = function(name, type, record, units) {
    this.type   = type;
    this.record = record;
    this.units  = units;
    this.name   = name
};

var maxPrice = 500;

var TradeEngine = function(opt) {
    (_.isObject(opt) || (opt = {}));

    this.live = opt.live || false;
    this.balance = this.live ? global.account.balance : 1500;
    this.startBalance = this.balance;
    this.balanceValue = this.balance;

    this.orders = [];
    this.profit = 0;
    this.rate = 0;
    this.history = {};
};

TradeEngine.prototype =  {
    canBuyAmount: function(price) {
        if (this.balance > 200) {
            return Math.floor(this.balance / price);
        } else {
            return 0;
        }
    },

    buy: function(name, type, record, callback) {
        var units = this.canBuyAmount(record.ask);

        if (units === 0) {
            callback('Not enough money');
            return;
        }

        if (_.find(this.orders, {name: name})) {
            callback('There already exists an open order for this instrument');
            return;
        }

        var order = new Trade(name, type, record, units);

        if (!this.history[name]) {
            this.history[name] = [];
        }

        if (this.live) {
            var self = this;

            this.realBuy(order, function(err, result) {
                if (err) {

                    console.log(err);

                } else {

                    order.id = result.tradeOpened.id;

                    self.balance -= (units * result.price);

                    self.updateStats(order);

                    self.history[name].push({
                        action: 'bought',
                        record: record,
                        units: units,
                        type: type,
                        balance: self.balance
                    });

                    self.orders.push(order);

                    callback(null, order);

                }
            });
        } else {

            this.balance -= (units * record.ask);

            this.history[name].push({
                action: 'bought',
                record: record,
                units: units,
                type: type,
                balance: this.balance
            });

            this.orders.push(order);

            this.updateStats();

            callback(null, order);

        }
    },

    realBuy: function(order, callback) {
        var orderOptions = {
            instrument: order.name.toUpperCase(),
            units: order.units,
            side: order.type === 'long' ? 'buy' : 'sell',
            type: 'market'
        };

        global.api.createOrder(global.accountId, orderOptions, function(err, data) {
            if (err) {
                callback(err, null);
            }
            else {
                console.log('Just bought!');

                callback(null, data);
            }
        });
    },

    sell: function(name, record, callback) {

        // Find the correct order.
        // TODO: Corresponding order should be passed to this
        var order = _.findWhere(this.orders, {name: name});

        if (!order) {
            throw new Error('No order found for instrument: ' + name);
        }

        // LIVE SITUATION
        if (this.live) {

            var self = this;

            this.realSell(order, function(err, data) {

                if (err) {

                    console.log(err);

                } else {

                    self.profit += data.profit;

                    self.balance += (data.price * order.units);

                    self.history[name].push({
                        name: name,
                        action: 'sold',
                        record: record,
                        units: order.units,
                        type: order.type,
                        balance: self.balance,
                        profit: self.profit
                    });

                    self.orders = _.reject(self.orders, {name: name});

                    self.updateStats();

                    callback(null, self.profit);
                }

            });

        // EMULATED SITUATION
        } else {

            if (order.type === 'long') {

                var result = (record.ask - order.record.ask) * order.units;
                this.balance += (record.ask * order.units);

            } else {

                var result = (order.record.ask - record.ask) * order.units;
                this.balance += (record.ask * order.units) + (result * 2);

            }

            this.profit += result;

            this.history[name].push({
                name: name,
                action: 'sold',
                record: record,
                units: order.units,
                type: order.type,
                balance: this.balance,
                profit: result
            });

            this.orders = _.reject(this.orders, {name: name});

            this.updateStats();

            callback(null, result);
        }
    },

    realSell: function(order, callback) {
        global.api.closeTrade(global.accountId, order.id, function(err, data) {
            if (err) {

                callback(err, null);

            } else {

                console.log('Just Sold');

                callback(null, data);

            }
        });
    },

    updateStats: function(order) {
        this.rate = ((this.profit / this.startBalance) * 100);

        if (this.orders.length) {
            this.balanceValue = parseFloat((this.balance + (this.orders[0].record.ask * this.orders[0].units)));
        } else {
            this.balanceValue = this.balance.toFixed(2);
        }
    }
};

module.exports = TradeEngine;