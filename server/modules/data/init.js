var _               = require('underscore');
var _BaseClass      = require(global.root + 'class/_base');

var Client          = require('./db/client');
var dbUtils         = require('./db/utils');

function DatabaseModule () {
    return this;
}

DatabaseModule.prototype = Object.create(dbUtils, {
    client: {
        value: null,
        writable: true
    },

    connect: {
        value: function(callback) {
            var self = this;

            Client(function(err, client) {
                if (err) {
                    callback(err);
                } else {
                    self.client = client;

                    callback(null);
                }
            });
        }
    },

    refreshDB: {
        value: function(callback) {
            var self = this;

            this.createAllInstruments(function() {

                self.updateHistory(callback);

            });
        }
    },

    getHistory: {
        value: function(options, callback) {

            if (options && options.instruments) {

                this.getSpecificInstrumentsData(options.instruments, callback);

            } else {

                this.getAllInstrumentsData(callback);

            }
        }
    },

    getFakeTick: {
        value: function(instruments) {
            var self = this;

            this.getSpecificInstrumentsData(instruments, function(data) {

                var result = {};

                _.forEach(data, function(records, name) {
                    result[name] = _.shuffle(records)[0];
                });


                self.trigger('update', result);

            });
        }
    }
});

_.extend(DatabaseModule.prototype, _BaseClass);

module.exports = DatabaseModule;