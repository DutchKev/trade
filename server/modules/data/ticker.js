var _       = require('underscore');
var DBUtil  = require('./db/utils');
var later   = require('later');

module.exports = {

    start: function(opt) {
        this.setUpdateInterval();

    },

    timeIntervals: {
        'M15': [0, 15, 30, 45],
        'M30': [0, 30],
        'H1': [0]
    },

    setUpdateInterval: function() {
        var self = this;
        var lastMinuteUpdate = null;

        setInterval(function() {
            var now    = new Date();
            var minutes = now.getMinutes();

            var timeInterval = self.timeIntervals[global.symConfig.timeInterval];

            if (_.contains(timeInterval, minutes) && lastMinuteUpdate !== minutes) {

                lastMinuteUpdate = minutes;

                now.setSeconds(0);
                now.setMilliseconds(0);

                global.api.getInstruments(global.accountId, function(error, instruments) {

                    instruments = _.without(instruments, {halted: true});

                    instruments = _.map(instruments, function (table) {
                        return table.instrument.toUpperCase();
                    });

                    global.api.getPrice(instruments, function (err, records) {

                        var newRecords = {};

                        _.forEach(records, function (data) {

                            var instrument = data.instrument;

                            var currentTime = new Date();
                            var recordTime  = new Date(data.time);
                            var maxDistance = 1000 * 60 * 10; // 10 minutes

                            // Check if the current time is any near the current future
                            if (currentTime.getTime() - recordTime.getTime() < maxDistance) {

                                var record = {
                                    ask: data.ask,
                                    bid: data.bid,
                                    time: data.time,
                                    timestamp: new Date(data.time).getTime()
                                };

                                newRecords[instrument] = record;

                            } else {
                                //console.log('NOT')
                            }

                        });

                        if (!_.isEmpty(newRecords)) {
                            //self.trigger('data:update')
                        }

                        global.io.emit('data:update', newRecords);

                    });

                });
            }

        }, 1000);

    }
};
