var Client  = require('mariasql');
var client  = null;

module.exports = function(callback) {
    if (client instanceof Client) {
        return callback(null, client);
    }

    var client = new Client();

    client.connect({
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: 'halo33221',
        db: 'trade',
        multiStatements: true
    });

    client.on('connect', function() {
        callback(null, client);
    });

    return client;
};