'use strict';

var _       = require('underscore');
var tools   = require(global.root + 'tools/tools');

module.exports = {
    getInstrumentsList: function(callback) {
        var tables = [];

        this.client.query('SHOW TABLES').on('result', function(res) {
            res.on('row', function(table) {

                // if table does not start with a '_', it is a trading table.
                if (table.Tables_in_trade[0] !== '_' && table.Tables_in_trade[0] !== 'undefined') {
                    tables.push(table.Tables_in_trade);
                }

            });
        }).on('end', function() {
            callback(tables);
        });
    },

    getLastRecord: function(instrument, callback) {
        var last = {};
        var query = 'SELECT * FROM ' + instrument + ' ORDER BY time DESC LIMIT 1';

        this.client.query(query).on('result', function(res) {
            res.on('row', function(data) {
                last = data;
            });
        }).on('end', function() {
            callback(last);
        });
    },

    getInstrumentData: function(opt, callback) {
        var self = this;

        var intervals = {
            '1M': '"%00%"',
            '5M': '"%0:00" OR time LIKE "%5:00"',
            '15M': '"%00:00" OR time LIKE "%15:00" OR time LIKE "%30:00" OR time LIKE "%45:00"',
            '30M': '"%00:00" OR time LIKE "%30:00"',
            '1H': '"%00:00"'
        };

        this.getLastRecord(opt.instrument, function(last) {

            var rows    = [];

            var lastRecordTime = (last.time && new Date(last.time) || new Date()).getTime() / 1000;

            var query   =
                'SELECT * ' +
                'FROM ' + opt.instrument + ' ' +
                'WHERE time > DATE_SUB(FROM_UNIXTIME(' + lastRecordTime + '), INTERVAL 3 MONTH) ' + // 3 months
                //'WHERE time > DATE_SUB(FROM_UNIXTIME(' + lastRecordTime + '), INTERVAL 148 HOUR) ' + // 1 week
                'AND (time LIKE ' + intervals[opt.interval || '15M'] + ') ' +
                'ORDER BY time DESC';

            //console.log(query);

            self.client.query(query).on('result', function(res) {
                res.on('row', function(row) {

                    // Convert timestamp to int
                    row.timestamp = parseInt(row.timestamp, 10);

                    rows.push(row);
                });
            }).on('end', function() {

                callback(self.prepareInstrumentData(rows).reverse());

            });

        });
    },

    getSpecificInstrumentsData: function(list, callback) {

        var AllData = {},
            self = this;

        (function getTableData() {
            var table = list.pop();

            self.getInstrumentData({
                instrument: table
            }, function(data) {
                AllData[table] = data;

                if (list.length) {
                    getTableData();
                } else {
                    callback(AllData);
                }
            });
        })();
    },

    getAllInstrumentsData: function(callback) {
        var AllData = {},
            self = this;

        var count = 0;

        this.getInstrumentsList(function(tables) {

            (function getTableData() {
                var table = tables.pop();

                self.getInstrumentData({
                    instrument: table
                }, function(data) {
                    AllData[table] = data;

                    if (tables.length) {
                        getTableData();
                    } else {
                        callback(AllData);
                    }
                });
            })();

        });
    },

    prepareInstrumentData: function(data) {
        var i = data.length;

        while(i--) {
            let record = data[i];

            // Create int from timestamp string.
            record.timestamp   = parseInt(record.timestamp, 10);

            record.ask         = parseFloat(record.ask);
            record.bid         = parseFloat(record.bid);

            data[i] = record;
        }

        return data;
    },

    fillTable: function(instrument, records, callback) {
        (_.isArray(records) || (records = [records]));

        var self = this;

        var counter = 0;

        //this.createInstrument(instrument, function() {

            _.forEach(records, function(data) {

                if (data.complete || _.isUndefined(data.complete)) {

                    if (/^\d+$/.test(data.time)) {
                        data.time = data.time / 1000000;
                    } else {
                        data.time = new Date(data.time).getTime();
                    }

                    var rowData = {
                        ask         : data.openAsk || data.ask,
                        bid         : data.openBid || data.bid,
                        time        : 'FROM_UNIXTIME(' + data.time + ')',
                        timestamp   : new Date(data.time).getTime() * 1000
                    };

                    var keys = Object.keys(rowData);
                    var values = _.values(rowData).join(',');

                    var query =
                        'INSERT IGNORE INTO ' + instrument +
                        ' (' + keys + ') VALUES (' + values + ');';

                    self.client.query(query).on('end', function(result) {

                        if (++counter === records.length) {

                            callback();

                        }

                    });
                } else {

                    if (++counter === records.length) {

                        callback();

                    }

                }

            });

        //});
    },

    deleteInstrument: function(instrument, callback) {
        var query = 'DROP TABLE ' + instrument;

        this.client.query(query).on('end', callback)
    },

    deleteAllInstruments: function(callback) {
        var self = this;

        this.getInstrumentsList(function(instruments) {

            (function loop() {

                var instrument = instruments.pop();

                if (instrument) {

                    self.deleteInstrument(instrument, loop);

                } else {

                    callback();

                }

            })();

        });
    },

    createInstrument: function(instrument, callback) {
        var self = this;

        this.getInstrumentsList(function(tables) {

            if (tables.indexOf(instrument) === -1) {

                var query = 'CREATE TABLE ' + instrument + ' LIKE _template;';

                self.client.query(query).on('end', callback);

            } else {

                callback();

            }

        });
    },

    createAllInstruments: function(callback) {
        var self = this;

        this.deleteAllInstruments(function() {

            global.api.getInstruments(global.accountId, function(err, instruments) {

                (function loop() {

                    var instrument = instruments.pop();

                    if (instrument) {

                        self.createInstrument(instrument.instrument, loop);

                    } else {

                        callback();

                    }

                })();

            });

        });
    },

    updateHistory: function(callback) {
        var self            = this;
        var timeInterval    = global.symConfig.timeInterval;

        global.api.getInstruments(global.accountId, function(error, instruments) {

            (function updateInstrumentData() {

                var instrument = instruments.pop().instrument;

                self.getLastRecord(instrument, function(record) {

                    // If no previous record exists, fake 3 months ago.
                    if (_.isEmpty(record)) {
                        record = {time: tools.getMonthsAgo(6)}
                    }

                    global.api.getCandles(instrument, record.time, new Date(), timeInterval, function(err, records) {

                        // TODO: repeat the call on timeout error (happens regularly...)
                        if (err) {
                            return console.log(err);
                        }


                        self.fillTable(instrument, records, function() {

                            if (instruments.length) {

                                console.log(instruments.length + ' to go!');

                                updateInstrumentData();

                            } else {

                                callback();

                            }

                        });

                    });
                });

            })();

        });
    }
};