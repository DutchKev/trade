'use strict';

module.exports = {

    _worker: null,

    _lastResult: null,

    get lastResult() {
        if (this._lastResult === null) {
            this.run();
        }

        return this._lastResult;
    },

    set lastResult(data) {
        this._lastResult = data;
    },

    turnTradingOn: function(frame) {
        var doAll = true,
            topInstruments = global.topInstruments;

        for (let name in frame.instruments) {
            frame.instruments[name].doTrades = !!(doAll || _.contains(topInstruments, name));
        }

    },

    turnTradingOff: function(frame) {
        for (let name in frame.instruments) {
            frame.instruments[name].doTrades = false;
        }
    },

    run: function () {
        var allData = {};

        var frame       = new Frame();
        var preTime     = tools.getMonthsAgo(6);
        var tradingStartTime    = tools.getMonthsAgo(5);
        var tradingUntilTime    = tools.getMonthsAgo(0);

        _.forEach(global.MainFrame.instruments, function(instrument, instrumentName) {
            var records = instrument.records;

            allData[instrumentName] = tools.getRecordsBetweenTimes(records, preTime);

            frame.addInstrument(instrumentName, [], false);
        });

        var firstRecords = allData[Object.keys(allData)[0]];

        var scanPeriod  = {
            from    : (firstRecords[0] && firstRecords[0].timestamp) || 0,
            until   : tradingStartTime
        };

        console.time("emulator");
        // Performance optimization, native loops
        // Saves a LOT of time.
        for (let i = 0, len = firstRecords.length; i < len; ++i) {

            let record = firstRecords[i];
            let currentTimestamp = record.timestamp;

            // Turn on trading after 1 week of 'scanning'
            if (currentTimestamp >= tradingStartTime && currentTimestamp <= tradingUntilTime) {
                this.turnTradingOn(frame);
            } else {
                this.turnTradingOff(frame);
            }

            let tickData = {};

            // Performance optimization, native loops
            // Loop over all instruments.
            for (let instrumentName in allData) {

                let data = allData[instrumentName],
                    i = data.length;

                // Loop over all data from instrument
                while (i--) {
                    if (data[i].timestamp === currentTimestamp) {
                        tickData[instrumentName] = data[i];
                        break;
                    }
                }

            }

            frame.tick(tickData);
        }

        console.timeEnd("emulator");

        this.lastResult = null;
        this.lastResult = frame.createResult(undefined, scanPeriod);

        frame.destroy();
        frame = null;
    }
};
