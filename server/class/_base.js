
module.exports = {
	/* ************** CUSTOM EVENTS ************ */
	on: function (name, callback, context) {

		name = name.replace(/ +(?= )/g,''); // remove all doubled spaces.
		(this._events || (this._events = [])).push({
			name: name,
			callback: callback,
			context: context || this,
			isTriggered: false
		});
		return this;
	},

	once: function (name, callback, context) {
		var self = this;

		this.on(name, function() {
			self.off(name);

			callback.apply(context, arguments);

		}, context)
	},

	trigger: function (name, data) {
		//console.log(this._events);
		if (typeof this._events === 'undefined' || typeof this._events.length === 'undefined' || this._events.length === 0) {
			return this;
		}

		name = name.replace(/ +(?= )/g,''); // remove all doubled spaces.

		// Remove the first attribute (the event name) from the arguments list,
		// So that all remaining attributes/variables can be passed to the callback function.
		var data = Array.prototype.slice.call(arguments);
		data.splice(0, 1);

		var events = this._events,
			triggerEvents = name.split(' '),
			t = triggerEvents.length,
			i, x, xkey;

		while (t--) {
			i = events.length;
			while (i--) {
				x = events[i].name.split(' ');
				xkey = x.length;
				while (xkey--) {
					if (x[xkey] === triggerEvents[t]) {
						events[i].isTriggered = true;
						events[i].callback.apply(events[i].context, data);
					}
				}
			}
		}
		return this;
	},

	off: function (name) {
		name = name.replace(/ +(?= )/g,''); // remove all doubled spaces.

		(this._events || (this._events = [])).forEach(function (event, index) {
			events.name === name && events.splice(index, 1);
		});

		return this;
	}
};
